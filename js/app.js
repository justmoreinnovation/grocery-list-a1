var myApp = angular.module('App1', []);

myApp.controller('Ctrl1', ['$scope', function($scope) {

    $scope.addItem = function(){
        if( $scope.currentItem.name &&  $scope.currentItem.quantity)
        {
            if(!$scope.edit)
            {
                $scope.list.push($scope.currentItem);
            }
            else
            {
                var index = $scope.findItemIndexById(id);
                $scope.list[index].name = $scope.currentItem.name;
                $scope.list[index].quantity = $scope.currentItem.quantity;
            }

            $scope.saveList();
            $('#addItem').modal('hide')
        }
    };

    $scope.deleteItem = function () {
        var index = $scope.findItemIndexById($scope.currentItem.id);

        if(index)
        {
            $scope.list.splice(index, 1);
        }
        $('#addItem').modal('hide')
    };

    $scope.showDialog = function(id){
        if(id)
        {


        //    var index = $scope.findItemIndexById(id);
            $scope.edit = true;
            $scope.currentId = id;
//            if(index >= 0)
//            {
////                $scope.currentItem = $scope.list[index];
//            }
        }
        $('#addItem').modal('show')
    };

    $scope.findItemIndexById = function(id){
        var index = -1;
        for(var i= 0,iMax=$scope.list.length;i<iMax;i++)
        {
            if($scope.list[i].id == id)
            {
                index = i;
                break;
            }
        }

        return index;
    };


    $scope.getNewPlaceholderItem = function(){
        return {
            name: '',
            quantity: '',
            checked: false,
            id: $scope.id++
        };
    };

    $scope.saveList = function(){
        if (localStorage){
            localStorage.list = JSON.stringify($scope.list);
        }
    };

    $scope.loadList = function(){
        if (localStorage){
            $scope.list = JSON.parse(localStorage.list);

            for(var i=0,iMax=$scope.list.length;i<iMax;i++){
                var current = $scope.list[i].id;
                if( current > $scope.id)
                {
                    $scope.id = current+1;
                }
            }
        }
    };
    if (localStorage && localStorage.list){
        $scope.loadList();
    }
    else{
        $scope.id = 1001;

        $scope.list = [
                {
                    id: 2,
                    checked : true,
                    name : 'Apples',
                    quantity: 4
                },
                {
                    id: 1,
                    checked: false,
                    name: 'Oranges',
                    quantity: 2
                }

            ];
    }


    $scope.currentItem = $scope.getNewPlaceholderItem();
    $scope.edit = false;
    $scope.editMap = {};


    $('#addItem').on('hidden.bs.modal', function () {
        if($scope.findItemIndexById($scope.currentItem.id) >= 0)
        {
            $scope.currentItem = $scope.getNewPlaceholderItem();
        }
        $scope.edit = false;
    })




}]);